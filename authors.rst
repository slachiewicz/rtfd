############
Contributors
############

Authors of **Oracle SQL & PL/SQL Optimization for Developers** are (in order of first commit date):

* Ian Hellström
